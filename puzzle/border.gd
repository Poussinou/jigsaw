extends Node

@export var Borders: Array[Curve2D]

func randomize_curve(border_curve: Curve2D):
	# expect 4 points, move inner ones by small amount
	for i in range(1,3):
		var pos = border_curve.get_point_position(i)
		pos.x += (randi() % 40)/10.0 - 2
		pos.y += (randi() % 40)/10.0 - 2
		border_curve.set_point_position(i, pos)
	return border_curve

func get_new_border() -> PackedVector2Array:
	var index = randi() % Borders.size()
	var border_curve: Curve2D = Borders[index].duplicate()
	border_curve = randomize_curve(border_curve)
	var border = border_curve.get_baked_points()
	if randi() % 2:
		border = get_y_flipped_border(border)
	if randi() % 2:
		border = get_x_flipped_border(border)
	return border

func get_x_flipped_border(border_points: PackedVector2Array):
	for i in range(border_points.size()):
		border_points[i] = Vector2(-border_points[i].x + Global.piece_size,
									border_points[i].y)
	border_points.reverse()
	return border_points

func get_y_flipped_border(border_points: PackedVector2Array):
	for i in range(border_points.size()):
		border_points[i] = Vector2(border_points[i].x, -border_points[i].y)
	return border_points

func get_left_border(border_points: PackedVector2Array):
	for i in range(border_points.size()):
		border_points[i] = border_points[i].rotated(PI/2)
	border_points.reverse()
	return border_points

func get_right_border(border_points: PackedVector2Array):
	border_points = get_left_border(border_points)
	for i in range(border_points.size()):
		border_points[i] += Vector2(Global.piece_size, 0)
	border_points.reverse()
	return border_points

func get_bottom_border(border_points: PackedVector2Array):
	for i in range(border_points.size()):
		border_points[i] += Vector2(0, Global.piece_size)
	border_points.reverse()
	return border_points
