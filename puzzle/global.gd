extends Node

@onready var root = $"/root"
@onready var area = $"../UI/PuzzlePanel/CenterContainer/Panel/PuzzleManager"
@onready var placed = $"../UI/PuzzlePanel/CenterContainer/Panel/PuzzleManager/PuzzleLocation"
@onready var setup_panel = $"../UI/SetupMenu"
@onready var pause_button = $"../UI/pause_button"

var piece_size = 64
var pieces_by_touch_id = [null]


# Puzzle Data:
var pieces_x = 2
var pieces_y = 2

var texture: Resource
var picture_path = "res://icon.svg"
#OS.get_system_dir(OS.SYSTEM_DIR_DESKTOP)
var picture_directory = ""
var connect_diagonal = false
var rotation_enabled = false

func set_screen_stretch(min_screen: Vector2):
	get_tree().root.content_scale_size = min_screen
