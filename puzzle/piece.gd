class_name Piece
extends Control

var follow_touch_id := -1
var distance_moved := 0.0
var border := [
	PackedVector2Array(),
	PackedVector2Array(),
	PackedVector2Array(),
	PackedVector2Array(),
	]
@onready var neighbor_pieces := []

enum {IN_CONTAINER, ONBOARD, CONNECTED, PLACED}
var state_connected := ONBOARD

func setup(texture: Texture2D, pic_scale: Vector2,
			pic_position: Vector2, pic_offset: Vector2):
	$marker.position = -pic_position
	$texture.texture_offset = pic_position + pic_offset
	$texture.texture_scale = pic_scale
	$texture.texture = texture

func connect_piece(piece: Piece):
	piece.remove_from_group("placeable_piece")

	var saved_pos = piece.global_position
	get_parent().remove_child(piece)
	$connected_pieces.add_child(piece)
	piece.global_position = saved_pos + get_rotated_pos(Vector2(0,0))

	piece.state_connected = CONNECTED

	for old_child in piece.get_node("connected_pieces").get_children():
		piece.get_node("connected_pieces").remove_child(old_child)
		$connected_pieces.add_child(old_child)
		old_child.position += piece.position

	piece.set_enabled(false)
	piece.set_process_input(false)

	for new_neighbor in piece.neighbor_pieces:
		if new_neighbor in neighbor_pieces: continue
		neighbor_pieces.append(new_neighbor)
	$animation_player.play("pop")

func generate_border():
	var points = PackedVector2Array()
	points.append(Vector2(0,0))
	points.append_array(border[0])
	points.append(Vector2(64,0))
	points.append_array(border[1])
	points.append(Vector2(64,64))
	points.append_array(border[2])
	points.append(Vector2(0,64))
	points.append_array(border[3])
	$texture.polygon = points
	points.append(Vector2(0,0))
	$outline.set_points(points)

func set_enabled(enable: bool):
	self.get_node("marker").monitorable = enable
	self.get_node("marker").monitoring = enable

func rotate_piece():
	if !Global.rotation_enabled: return
	$marker.set_monitoring(false)
	rotation += PI/2
	if rotation > 2*PI - .1: rotation = 0
	$marker.set_monitoring(true)
	check_match()

func place():
	#piece.scale = scale
	position -= Global.placed.global_position
	Global.placed.add_child(self)
	self.disconnect("gui_input", Callable(self, "_on_piece_gui_input"))
	# negative seek (duration from maker distance) and raise
	$animation_player.play("pop")

	$marker.queue_free()
	for group in get_groups():
		if group == "piece": continue
		remove_from_group(group)

func _input(event):
	#event.position is global
	if follow_touch_id < 0: return
	if event is InputEventScreenDrag:
		if event.index != follow_touch_id: return
		distance_moved += event.relative.length_squared()
		position += event.relative
	elif event is InputEventScreenTouch:
		if event.index != follow_touch_id: return
		if event.pressed: return
		#release
		follow_touch_id = -1
		Global.pieces_by_touch_id[follow_touch_id] = null
		if distance_moved < 2:
			rotate_piece()
		await get_tree().create_timer(.1).timeout
		check_match()

func check_match():
	if rotation < .1 and Global.area in $marker.get_overlapping_areas():
		position = Global.placed.global_position - $marker.position
		for piece in $connected_pieces.get_children():
			$connected_pieces.remove_child(piece)
			#piece.position *= scale
			piece.position += position
			#piece.rect_position += Vector2.ONE * Global.piece_size / 2
			piece.place()
		get_parent().remove_child(self)
		set_process_input(false)
		place()
		return

	for piece_marker in $marker.get_overlapping_areas():
		if piece_marker == Global.area: continue
		var connecting_piece = piece_marker.get_parent()
		# check for same rotation
		if not abs(connecting_piece.rotation - rotation) < .1:
			continue
		# check if marked as possible neighbor
		if not connecting_piece in neighbor_pieces and \
			not self in connecting_piece.neighbor_pieces: continue

		# set piece to fit perfectly on soon to be connected piece
		position += piece_marker.global_position - $marker.global_position
		connecting_piece.rotation = 0
		connect_piece(connecting_piece)
		#return

func get_rotated_pos(local_pos: Vector2):
	# rotates the vector according of rect_rotation
	if rotation_degrees > 270 - .1:
		return Vector2(local_pos.y, - local_pos.x + 64)
	if rotation_degrees > 180 - .1:
		return Vector2(- local_pos.x + 64, - local_pos.y + 64)
	if rotation_degrees > 90 - .1:
		return Vector2(- local_pos.y + 64, local_pos.x)
	return Vector2(local_pos.x, local_pos.y)

func _on_piece_gui_input(event):
	#event.position is local
	if event is InputEventScreenTouch:
		if state_connected == CONNECTED:
			event.position += position
			get_parent().get_parent()._on_piece_gui_input(event)
			return
		if follow_touch_id > -1: return
		if not event.pressed: return
		distance_moved = 0
		follow_touch_id = event.index

		#if get_parent() == Global.area.get_node("pieces"):
		if state_connected == ONBOARD:
			move_to_front()
			Global.pieces_by_touch_id[follow_touch_id] = self
