extends Node2D

const piece_tscn = preload("res://puzzle/piece.tscn")
var picture: Texture2D

var piece_scale: Vector2
var offset: Vector2

signal puzzle_solved

func _ready():
	Global.setup_panel.set_enabled(true)
	# warning-ignore:return_value_discarded
	get_tree().get_root().connect("size_changed", Callable(self, "keep_pieces_onscreen"))

func keep_pieces_onscreen():
	if Global.setup_panel.visible: return
	var visible_size = get_viewport().get_visible_rect().size
	var visible_piece_size = visible_size - global_position \
							- Vector2.ONE * Global.piece_size
	for piece in $Pieces.get_children():
		piece.position.x = clamp(piece.position.x,
										-global_position.x,
										visible_piece_size.x)
		piece.position.y = clamp(piece.position.y,
										-global_position.y,
										visible_piece_size.y)

func setup_puzzle():
	# remove old pieces
	get_tree().call_group("piece", "queue_free")
	# load new puzzle
	setup_screen()
	load_image(Global.picture_path)
	generate_pieces()
	# scramble pieces after screen resize complete
	call_deferred("scramble_pieces")

func setup_screen():
	var puzzle_area = Vector2(Global.pieces_x, Global.pieces_y + 4)
	puzzle_area *= Global.piece_size
	puzzle_area += Vector2.ONE * 5
	var puzzle_size = Vector2(Global.pieces_x, Global.pieces_y)
	puzzle_size *= Global.piece_size
	Global.set_screen_stretch(puzzle_area)
	get_parent().custom_minimum_size = puzzle_size

func scramble_pieces():
	var real_size = get_viewport().get_visible_rect().size
	for piece: Piece in $Pieces.get_children():
		piece.position = - global_position
		piece.position.x += randi() % int(real_size.x - 64)
		piece.position.y += randi() \
			% int((real_size.y - Global.piece_size * Global.pieces_y) / 2 - 64)
		if (randi() % 2):
			piece.position.y *= -1
			piece.position.y += Global.piece_size * Global.pieces_y - 64

func load_image(path: String):
	var image = Image.new()
	var err = image.load(path)
	if err != OK:
		print("load failed")

	picture = ImageTexture.create_from_image(image)
	piece_scale = image.get_size()
	piece_scale /= Global.piece_size
	piece_scale /= Vector2(Global.pieces_x,Global.pieces_y)
	if piece_scale.x < piece_scale.y:
		piece_scale.y = piece_scale.x
		offset = Vector2.ZERO
		offset.y = image.get_size().y
		offset.y -= Global.piece_size * piece_scale.x * Global.pieces_y
	else:
		piece_scale.x = piece_scale.y
		offset = Vector2.ZERO
		offset.x = image.get_size().x
		offset.x -= Global.piece_size * piece_scale.y * Global.pieces_x
	offset /= piece_scale.x * 2

func set_neighbours(piece1: Piece, piece2: Piece):
	piece1.neighbor_pieces.append(piece2)
	piece2.neighbor_pieces.append(piece1)

func generate_pieces():
	var pieces = []
	var y_max = Global.pieces_y
	var x_max = Global.pieces_x
	for y in range(y_max):
		for x in range(x_max):
			var pos = Vector2(0,64) * y + Vector2(64,0) * x
			var new_piece: Piece = piece_tscn.instantiate()
			$Pieces.add_child(new_piece)
			#new_piece.attach_to_container()
			new_piece.setup(picture, piece_scale, pos, offset)
			pieces.append(new_piece)
			if !y or y == y_max - 1 or !x or x == x_max - 1:
				new_piece.add_to_group("border_piece")
			else:
				new_piece.add_to_group("center_piece")
			new_piece.add_to_group("placeable_piece")
			new_piece.add_to_group("piece")
	#get_tree().set_group("center_piece", "visible", 0)
	#Global.container.randomize_children()

	#connect all 8 neighbours, up to 4 at a time
	for i in range(len(pieces)-1):
		if (i+1) % x_max: set_neighbours(pieces[i], pieces[i + 1])
		if i >= (y_max - 1) * x_max: continue
		set_neighbours(pieces[i], pieces[i + x_max])
		if not Global.connect_diagonal: continue
		if (i) % x_max: set_neighbours(pieces[i], pieces[i + x_max - 1])
		if (i+1) % x_max: set_neighbours(pieces[i], pieces[i + x_max + 1])

	# setup + create borders
	for i in range(len(pieces)-1):
		if (i+1) % x_max:
			var border: PackedVector2Array = $BorderManager.get_new_border()
			pieces[i].border[1] = \
				$BorderManager.get_right_border(border.duplicate())
			pieces[i+1].border[3] = \
				$BorderManager.get_left_border(border.duplicate())

		if i < (y_max - 1) * x_max:
			var border = $BorderManager.get_new_border()
			pieces[i].border[2] = \
				$BorderManager.get_bottom_border(border.duplicate())
			pieces[i+x_max].border[0] = \
				border.duplicate()
	for piece in pieces:
		piece.generate_border()

	for piece in pieces:
		for _i in range(randi() % 4):
			piece.rotate_piece()


func _on_pieces_child_exiting_tree(_node):
	if len($Pieces.get_children()) - 1 == 0:
		puzzle_solved.emit()
