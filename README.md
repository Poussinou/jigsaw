# Jigsaw
This is a simple jigsaw puzzle game, designed mainly for multi-touch devices.
To get started, simply enter the path
to a folder and choose your desired picture.
Have fun solving the puzzle after generating one!

The game is designed with a straightforward interface,
emphasizing ease of use.
Enjoy the game without interruptions,
free from ads and other annoyances.

The game is developed exclusively using GDScript within the Godot 4 engine.
A goal is to write the source code in such a way
that it can be easily understood and modified.
Feel free to explore the source code,
report bugs, and contribute to the project.

## License
Copyright (c) 2024 Josef Ott

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

## Godot
This project uses Godot Engine, released under the permissive MIT license.
For detailed license information, visit <https://godotengine.org/license>.
Check out the source code here: <https://github.com/godotengine/godot>.
Copyright (c) 2014-present Godot Engine contributors.
Copyright (c) 2007-2014 Juan Linietsky, Ariel Manzur.
