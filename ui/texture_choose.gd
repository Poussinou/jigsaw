extends VBoxContainer

var files = []
var currently_selected

func load_image(path: String):
	print(path)
	var image = Image.new()
	var err = image.load(path)
	if err != OK:
		print("load failed " + str(err))
	err = FileAccess.file_exists(path)
	image.resize(80,80)
	var pic = ImageTexture.create_from_image(image) #,4
	return pic

func load_puzzle_pictures():
	files.shuffle()
	for i in range(3):
		var picture_button: TextureButton = $HBoxContainer2.get_child(i)
		picture_button.visible = false
	if len(files) == 0:
		$HBoxContainer2/placeholder_button.visible = true
		currently_selected = $HBoxContainer2.get_child(3)
		Global.picture_path = "res://icon.svg"
		return
	else:
		$HBoxContainer2/placeholder_button.visible = false
	for i in range(min(3, len(files))):
		var texture = load_image(files[i])
		var picture_button: TextureButton = $HBoxContainer2.get_child(i)
		picture_button.texture_normal = texture
		picture_button.visible = true
		picture_button.modulate = Color(.8,.8,.8)
	_on_puzzle_button_pressed(0)

func _ready() -> void:
	%placeholder_button.texture_normal = load_image("res://icon.svg")

func _on_puzzle_button_pressed(i):
	if i >= len(files): return
	if currently_selected:
		# revert modulation of old selection
		currently_selected.modulate = Color(.8,.8,.8)
	$HBoxContainer2.get_child(i).modulate = Color(1,1,1)
	currently_selected = $HBoxContainer2.get_child(i)
	Global.picture_path = files[i]

func _on_picture_directory_changed():
	%LineEdit.text = \
		$FileDialog.dir.get_current_dir()

func _on_line_edit_gui_input(event):
	if event is InputEventScreenTouch:
		if not event.pressed: return
		$FileDialog.visible = true

func _on_line_edit_item_rect_changed():
	$FileDialog.offset_top = %LineEdit.global_position.y + %LineEdit.size.y

func _on_file_dialog_picture_directory_changed(new_dir):
	Global.picture_directory = new_dir
	%LineEdit.text = new_dir
	files = $FileDialog.get_dir_contents()
	if not files:
		print("no pictues found")
		files = []
	load_puzzle_pictures()

func _on_line_edit_text_submitted(new_text):
	$FileDialog.visible = false
	$FileDialog.change_dir(new_text)
	#Global.picture_directory = $FileDialog.dir.get_current_dir()
	#_on_file_dialog_picture_directory_changed(new_text)


func _on_information_pressed() -> void:
	$FileDialog.visible = false
