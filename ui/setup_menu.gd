extends Panel

var subwindows_visible: bool = false:
	set = _set_subwindows_visible,
	get = _get_subwindows_visible

func _get_subwindows_visible() -> bool:
	if %Information/Panel.visible or %TextureChoose/FileDialog.visible:
		return true
	return false

func _set_subwindows_visible(subwindows_stay_open: bool) -> void:
	if subwindows_stay_open: return
	%Information/Panel.visible = false
	%TextureChoose/FileDialog.visible = false

func set_enabled(enable: bool):
	visible = enable
	#Global.area.visible = !enable
	Global.area.get_parent().visible = !enable
	#Global.pause_button.visible = !enable
	Global.rotation_enabled = %RotationButton.button_pressed

	if not enable: return
	Global.set_screen_stretch(Vector2(295,500))

func _on_check_box_toggled(button_pressed):
	Global.rotation_enabled = button_pressed
	if button_pressed:
		%RotationButton.text = "ROTATION_ENABLED"
	else:
		%RotationButton.text = "ROTATION_DISABLED"

func _on_pause_button_pressed():
	set_enabled(true)

func _on_back_button_pressed():
	set_enabled(false)
	Global.area.setup_screen()

func _on_generate_button_pressed():
	Global.pieces_x = %SizeChoose.x
	Global.pieces_y = %SizeChoose.y
	Global.area.setup_puzzle()
	set_enabled(false)
	%BackButton.visible = true
