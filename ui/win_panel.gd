extends Panel

func _on_button_pressed():
	visible = false
	Global.setup_panel.set_enabled(true)

func _on_puzzle_manager_puzzle_solved():
	visible = true
