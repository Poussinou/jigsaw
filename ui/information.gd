extends Button

func set_info_text():
	var file = FileAccess.open("res://README.md", FileAccess.READ)
	var content = file.get_as_text()

	# remove unnecessary newlines
	var regex = RegEx.new()
	regex.compile(r"(?<=\n)([a-zA-Z]+.*)\n(?=\S)")
	content = regex.sub(content, "$1 ", true)

	%Label.text = content

func _ready():
	$Panel.visible = false
	set_info_text()

func _on_pressed():
	$Panel.visible = true

func _on_button_pressed():
	$Panel.visible = false
