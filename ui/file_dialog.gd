extends Panel

var dir := DirAccess.open("/")
signal picture_directory_changed(new_dir)

func _ready():
	if not OS.request_permissions():
		assert(false, "no permissions")
	dir.include_navigational = true
	dir.include_hidden = true
	#if dir.dir_exists(Global.picture_directory):
	#	dir.change_dir(Global.picture_directory)
	#else:
	dir.change_dir(OS.get_system_dir(OS.SYSTEM_DIR_DCIM))
	change_dir(".")

func get_dir_contents():
	var files = []
	var current_dir = dir.get_current_dir()
	dir.list_dir_begin() # TODOConverter3To4 fill missing arguments https://github.com/godotengine/godot/pull/40547
	while true:
		var file_name = dir.get_next()
		if file_name == "": break
		if dir.current_is_dir(): continue
		if file_name.get_extension().to_lower() in [
			"webp",
			"png",
			"jpg",
			"jpeg",
			]:
			files.append(current_dir.path_join(file_name))
	return files

func change_dir(directory):
	dir.change_dir(directory)
	var num_pictures = len(get_dir_contents())
	$v_box_container/label2.text = "\"{0}\"".format([dir.get_current_dir()])
	print(dir.get_current_dir())
	$v_box_container/label3.text = tr("CONTAINS_PICTURE_AMOUNT").format([num_pictures])
	generate_folder_buttons()
	picture_directory_changed.emit(dir.get_current_dir())

func new_folder_button(directory):
	var button = Button.new()
	button.text = " " + directory
	button.size_flags_horizontal = SIZE_EXPAND_FILL
	button.connect("pressed", Callable(self, "change_dir").bind(directory))
	button.clip_text = true
	button.alignment = HORIZONTAL_ALIGNMENT_LEFT
#	button.instance()
	$v_box_container/scroll_container/grid_container.add_child(button)

func generate_folder_buttons():
	for child in $v_box_container/scroll_container/grid_container.get_children():
		$v_box_container/scroll_container/grid_container.remove_child(child)
		child.queue_free()

	if OS.get_name() != "Linux":
		new_folder_button("..")
	dir.list_dir_begin()
	while true:
		var file_name = dir.get_next()
		if file_name == "": break
		if file_name == ".": continue
		if not dir.current_is_dir(): continue
		new_folder_button(file_name)


func _on_button_pressed():
	if dir.get_current_dir() != Global.picture_directory:
		Global.picture_directory = dir.get_current_dir()
		emit_signal("picture_directory_finalized")
	visible = false
